# Hex Converter

A simple command-line utility for 2-way conversion between BigInt and Hex strings.

## Build & Install

Build and install the local repo:
```shell
cargo install --path .
```

This will install a `hex` executable into your cargo bin dir (`.cargo/bin` by default on linux).

## Pre-compiled Binaries

- Linux: [bin/linux/hex](bin/linux/hex)
- Mac: [bin/mac/hex](bin/mac/hex)
- Windows: [bin/windows/hex.exe](bin/windows/hex.exe)

## Usage

```sh
# numbers will be parsed and converted to hex strings
shane@Shanes-Desktop ~/c/u/hex-converter (master)> hex 25
0x19

# prefix numbers with 0x to coerce parsing as hex
shane@Shanes-Desktop ~/c/u/hex-converter (master)> hex 0x19
25
```

```sh
shane@Shanes-Desktop ~/c/u/hex-converter (master)> hex 1045022
0xFF21E

# a string with non-digits will be inferred as a hex string
shane@Shanes-Desktop ~/c/u/hex-converter (master)> hex ff21e
1045022
```
