use num_bigint::BigUint;
use std::env;

fn main() {
    if env::args().count() != 2 {
        eprintln!("HEX: Please provide exactly one numerical or hex argument!");
        return;
    }

    let mut arg = &env::args().nth(1).unwrap().to_lowercase().replace(",", "")[..];
    let mut is_hex = false;

    if arg.starts_with("0x") {
        is_hex = true;
        arg = &arg[2..];
    }

    for c in arg.chars() {
        if !c.is_ascii_hexdigit() {
            eprintln!("HEX: Invalid hex argument!");
            return;
        }

        is_hex = is_hex || !c.is_ascii_digit();
    }

    let radix = if is_hex { 16 } else { 10 };
    let big_int = BigUint::parse_bytes(arg.as_bytes(), radix).unwrap();

    if is_hex {
        println!("{}", big_int);
    } else {
        println!("0x{:X}", big_int);
    }
}
